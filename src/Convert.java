import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;


public class Convert {

	public static void convert(File inFile, File outFile) throws IOException, XMLStreamException {

        FileOutputStream fos = new FileOutputStream(outFile + ".kmz");
        ZipOutputStream zip = new ZipOutputStream(fos);
        ZipEntry entry = new ZipEntry(outFile + ".kml");
        zip.putNextEntry(entry);
		
        PrintWriter dataWriter = new PrintWriter(zip);
        XMLStreamWriter xsw = XMLOutputFactory.newInstance().createXMLStreamWriter(dataWriter);
        xsw.writeStartDocument();

        xsw.setDefaultNamespace("http://www.opengis.net/kml/2.2");
        xsw.writeStartElement("kml");
            xsw.writeNamespace("gx", "http://www.google.com/kml/ext/2.2");
            xsw.writeNamespace(null, "http://www.opengis.net/kml/2.2");
            xsw.writeNamespace("atom", "http://www.w3.org/2005/Atom");

        xsw.writeStartElement("Document");

        xsw.writeStartElement("Folder");

        xsw.writeStartElement("Style");
	        xsw.writeStartElement("ListStyle");
		        xsw.writeStartElement("listItemType");
		        xsw.writeCharacters("radioFolder");
		        xsw.writeEndElement();
	        xsw.writeEndElement();
        xsw.writeEndElement();

        xsw.writeStartElement("Folder");

        
        xsw.writeStartElement("name");
        	xsw.writeCharacters("Humidity");
        xsw.writeEndElement();

        double lowPres = Double.MAX_VALUE, highPres = Double.MIN_VALUE; // remember pressure min and max for plotting later
        
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
		String line = reader.readLine();
        double[] curData = null;
        double[] newData = new double[8];
        while(null != line){
        	if(line.startsWith("#")) {
                line = reader.readLine();
        		continue;
        	}
            Scanner sc = new Scanner(line);
            sc.useDelimiter(",");
            long epochTime = sc.nextLong();
            for(int i=0; i<newData.length; i++) {
            	newData[i] = sc.nextDouble();
            }
            line = reader.readLine();

            if(curData != null){
            	
            	if(curData[3] > highPres)
            		highPres = curData[3];
            	if(curData[3] < lowPres)
            		lowPres = curData[3];
            	
                float hue = (float)((270.0 - curData[2] / 100.0 * 180.0) / 360.0);
                int color = Color.HSBtoRGB(hue, 1f, 1f);
                String hex = String.format("%06x", color);
                hex = hex.substring(6, 8) + hex.substring(4, 6) + hex.substring(2, 4);
                String lineColor = "ff" + hex;

	        	xsw.writeCharacters("\n");
	        	xsw.writeStartElement("Placemark");
	
	            xsw.writeStartElement("Style");
		            xsw.writeStartElement("LineStyle");
			            xsw.writeStartElement("color");
		        			xsw.writeCharacters(lineColor);
//		        			xsw.writeCharacters("ff00ffff");
			        	xsw.writeEndElement();
			            xsw.writeStartElement("width");
			        		xsw.writeCharacters("5");
			        	xsw.writeEndElement();
		        	xsw.writeEndElement();
	        	xsw.writeEndElement();
		        xsw.writeStartElement("LineString");
		            xsw.writeStartElement("extrude");
		        		xsw.writeCharacters("0");
		        	xsw.writeEndElement();
		            xsw.writeStartElement("altitudeMode");
		        		xsw.writeCharacters("absolute");
		        	xsw.writeEndElement();
		            xsw.writeStartElement("coordinates");
		        		xsw.writeCharacters("\n"+curData[6]+","+curData[5]+","+curData[7]+"\n"+newData[6]+","+newData[5]+","+newData[7]+"\n");
		        	xsw.writeEndElement();
		    	xsw.writeEndElement();
	
	            xsw.writeEndElement();//Placemark
            }
            
            if(null == curData)
            	curData = new double[newData.length];
            System.arraycopy(newData, 0, curData, 0, newData.length);
        
        }
        reader.close();
        
        xsw.writeEndElement();//Folder

        System.out.println("Pressure range: "+lowPres+" - "+highPres);
        
        xsw.writeStartElement("Folder");

        xsw.writeStartElement("name");
        	xsw.writeCharacters("Pressure");
        xsw.writeEndElement();

        reader = new BufferedReader(new FileReader(inFile));
		line = reader.readLine();
        curData = null;
        newData = new double[8];
        while(null != line){
        	if(line.startsWith("#")) {
                line = reader.readLine();
        		continue;
        	}
            Scanner sc = new Scanner(line);
            sc.useDelimiter(",");
            long epochTime = sc.nextLong();
            for(int i=0; i<newData.length; i++) {
            	newData[i] = sc.nextDouble();
            }
            line = reader.readLine();

            if(curData != null){
            	
            	double presRange = highPres-lowPres;
            	
                float hue = (float)((270.0 - (highPres-curData[3]) / presRange * 180.0) / 360.0);
                int color = Color.HSBtoRGB(hue, 1f, 1f);
                String hex = String.format("%06x", color);
                hex = hex.substring(6, 8) + hex.substring(4, 6) + hex.substring(2, 4);
                String lineColor = "ff" + hex;

	        	xsw.writeCharacters("\n");
	        	xsw.writeStartElement("Placemark");
	
	            xsw.writeStartElement("Style");
		            xsw.writeStartElement("LineStyle");
			            xsw.writeStartElement("color");
		        			xsw.writeCharacters(lineColor);
//		        			xsw.writeCharacters("ff00ffff");
			        	xsw.writeEndElement();
			            xsw.writeStartElement("width");
			        		xsw.writeCharacters("5");
			        	xsw.writeEndElement();
		        	xsw.writeEndElement();
	        	xsw.writeEndElement();
		        xsw.writeStartElement("LineString");
		            xsw.writeStartElement("extrude");
		        		xsw.writeCharacters("0");
		        	xsw.writeEndElement();
		            xsw.writeStartElement("altitudeMode");
		        		xsw.writeCharacters("absolute");
		        	xsw.writeEndElement();
		            xsw.writeStartElement("coordinates");
		        		xsw.writeCharacters("\n"+curData[6]+","+curData[5]+","+curData[7]+"\n"+newData[6]+","+newData[5]+","+newData[7]+"\n");
		        	xsw.writeEndElement();
		    	xsw.writeEndElement();
	
	            xsw.writeEndElement();//Placemark
            }
            
            if(null == curData)
            	curData = new double[newData.length];
            System.arraycopy(newData, 0, curData, 0, newData.length);
        
        }
        reader.close();
        
        xsw.writeEndElement();//Folder

        xsw.writeEndElement(); // Top folder

        
        xsw.writeEndElement();//Document
        xsw.writeEndElement();//kml
        
        xsw.close();
        zip.closeEntry();
        zip.close();
        fos.close();
        
		return;
	}
	
	public static void main(String[] args) throws IOException, XMLStreamException {
		Convert.convert(new File(args[0]), new File(args[1]));
	}

}
