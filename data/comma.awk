#!/usr/bin/awk
 
{
    if (NR == 1) { # Skip the first record which is the heading.
        print $0
    } else {
        printf ("%d,", $1);
        printf ("%.1f,", $2 "." $3);
        printf ("%.6f,", $4 "." $5);
        printf ("%.6f,", $6 "." $7);
        printf ("%.2f,", $8 "." $9);
        printf ("%.4f,", $(NF-6));
        printf ("%.6f,", $(NF-5) "." $(NF-4));
        printf ("%.6f,", $(NF-3) "." $(NF-2));
        printf ("%.6f", $(NF-1) "." $NF);
        print ""
    }
}